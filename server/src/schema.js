import {
  makeExecutableSchema,
  addMockFunctionsToSchema,
} from 'graphql-tools';

import { resolvers } from './resolvers';

const typeDefs = `

  input MessageInput{
    text: String
  }

  type Message {
    id: ID!
    text: String
  }

  type Query {
    messages: [Message]
  }

  type Mutation {
    addMessage(message: MessageInput!): Message
  }

  type Subscription {
    messageAdded: Message
  }

`;

const schema = makeExecutableSchema({ typeDefs, resolvers });
export { schema };