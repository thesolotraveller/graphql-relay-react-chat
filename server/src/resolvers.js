import { PubSub, withFilter } from 'graphql-subscriptions';
import { messages } from './messages';

let nextMessageId = 3;

const pubsub = new PubSub();

export const resolvers = {
  Query: {
    messages: () => messages
  },
  Mutation: {
    addMessage: (root, { message }) => {
      const newMessage = { id: String(nextMessageId++), text: message.text };
      messages.push(newMessage);
      pubsub.publish('NEW_MESSAGE_ADDED', { messageAdded: newMessage });
      return newMessage;
    }
  },
  Subscription: {
    messageAdded: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('NEW_MESSAGE_ADDED'),
        (payload, variables) => true
      )
    }
  }
};