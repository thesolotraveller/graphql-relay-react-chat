export const messages = [
  {
    id: '1',
    text: 'NodeJS is single threaded and works on non-blocking I/O',
  }, 
  {
    id: '2',
    text: 'React works on the idea of virtual DOM deep down',
  }
]