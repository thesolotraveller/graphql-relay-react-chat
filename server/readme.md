## this is the graphql server able to accept

* `query`
    {
        messages
    }

* `mutation`
    mutation addNewMsg {
        addMessage (message: {text: "Change is the only constant"}) {
            id
            text
        }
    }

* `subscription`
    subscription newMsgAdded {
        messageAdded {
            id
            text
        }
    }


## to start the graphql backend server
cd ~/server
npm start